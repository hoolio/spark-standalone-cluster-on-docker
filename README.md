# Introduction

This project gives you an **Apache Spark** cluster in standalone mode with a **JupyterLab** interface built on top of **Docker**.

Learn Apache Spark through its **Scala**, **Python** (PySpark) and **R** (SparkR) API by running the Jupyter [notebooks](build/workspace/) with examples on how to read, process and write data.

<br>
<p align="left"><img src="docs/image/cluster-architecture.png"></p>

Note this is not my work - it has been cloned from https://github.com/cluster-apps-on-docker/spark-standalone-cluster-on-docker

# Setup and install
## TL;DR
`sudo ./build.sh`
`sudo docker-compose up`

## Prerequisites

 - Install [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/), check Appendix: Tech Stack Defaults

## Build images (only supported on Linux OS distributions)

1. Download the source code or clone the repository

1. Note; images will be built using the default versions (see Appendix: Tech Stack defaults).  

    Optional; edit versions in the [build.yml](build/build.yml) and match those in the [docker compose](build/docker-compose.yml) file.

1. Build up the images;`chmod +x build.sh ; ./build.sh`

1. Start the cluster; `sudo docker-compose up`

1. Visit Jupyter at [localhost:8888](http://localhost:8888/) and note that the files in `./workspace/` are visible.  Feel free to add more!

1. Open the `overview.ipynb` to see csv content being loaded from file into spark and processed etc.

1. Stop the cluster by typing `ctrl+c` on the terminal, and again to force.

## Cluster overview

| Application     | URL                                      | Description                                                |
| --------------- | ---------------------------------------- | ---------------------------------------------------------- |
| JupyterLab      | [localhost:8888](http://localhost:8888/) | Cluster interface with built-in Jupyter notebooks          |
| Spark Driver    | [localhost:4040](http://localhost:4040/) | Spark Driver web ui                                        |
| Spark Master    | [localhost:8080](http://localhost:8080/) | Spark Master node                                          |
| Spark Worker I  | [localhost:8081](http://localhost:8081/) | Spark Worker node with 1 core and 512m of memory (default) |
| Spark Worker II | [localhost:8082](http://localhost:8082/) | Spark Worker node with 1 core and 512m of memory (default) |


# Appendix
## Attribution
all the smarts here cloned from https://github.com/cluster-apps-on-docker/spark-standalone-cluster-on-docker.
please see that repo to help thank contributors and how to become one etc etc.

## Tech Stack defaults
### Infrastructure 

| Component      | Version |
| -------------- | ------- |
| Docker Engine  | 1.13.0+ |
| Docker Compose | 1.10.0+ |

### Languages and Kernels

| Spark | Hadoop | Scala   | [Scala Kernel](https://almond.sh/) | Python | [Python Kernel](https://ipython.org/) | R     | [R Kernel](https://irkernel.github.io/) |
| ----- | ------ | ------- | ---------------------------------- | ------ | ------------------------------------- | ----- | --------------------------------------- |
| 3.x   | 3.2    | 2.12.10 | 0.10.9                             | 3.7.3  | 7.19.0                                 | 3.5.2 | 1.1.1                                   |
| 2.x   | 2.7    | 2.11.12 | 0.6.0                              | 3.7.3  | 7.19.0                                 | 3.5.2 | 1.1.1                                   |

### Apps

| Component      | Version                 | Docker Tag                                           |
| -------------- | ----------------------- | ---------------------------------------------------- |
| Apache Spark   | 2.4.0 \| 2.4.4 \| 3.0.0 | **\<spark-version>**                                 |
| JupyterLab     | 2.1.4 \| 3.0.0          | **\<jupyterlab-version>**-spark-**\<spark-version>** |